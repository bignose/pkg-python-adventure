Source: python-adventure
Maintainer: Ben Finney <bignose@debian.org>
Section: games
Priority: optional
Build-Depends:
    python3-setuptools,
    python3,
    dh-python,
    debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: https://github.com/brandon-rhodes/python-adventure/
VCS-Git: https://salsa.debian.org/bignose/pkg-python-adventure.git
VCS-Browser: https://salsa.debian.org/bignose/pkg-python-adventure
Rules-Requires-Root: no

Package: colossal-cave-adventure
Architecture: all
Depends:
    python3,
    ${python3:Depends},
    ${misc:Depends}
Provides: adventure
Description: Colossal Cave Adventure game
 Explore Colossal Cave, where others have found fortunes in treasure and
 gold, though it is rumored that some who enter are never seen again.
 .
 Colossal Cave Adventure (originally named “ADVENT” or “Adventure”) is
 the seminal text adventure game, written by Will Crowther and Don
 Woods.
 .
 This is a re-implementation of the “350-point” version, using the
 same game content from the PDP-10 source code of the late 1970s.
 .
 It uses the original text exactly, and emits responses slow enough to
 read as the contemporary terminal interfaces did.
